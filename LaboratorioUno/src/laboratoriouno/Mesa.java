/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Mesa {

    private int cHamSen;
    private int cHamQue;
    private int cHamEsp;
    private int cPapas;
    private int cRefresco;
    private int cPostre;
    
    private final int pHamSen;
    private final int pHamQue;
    private final int pHamEsp;
    private final int pPapas;
    private final int pRefresco;
    private final int pPostre;

    public Mesa() {
        
        pHamSen = 15;
        pHamQue = 18;
        pHamEsp = 20;
        pPapas = 8;
        pRefresco = 5;
        pPostre = 6;
    }

    public void capturarOrden(int num, int can) {
        switch (num) {
            case 1:
                cHamSen += can;
                break;
            case 2:
                cHamQue += can;
                break;
            case 3:
                cHamEsp += can;
                break;
            case 4:
                cPapas += can;
                break;
            case 5:
                cRefresco += can;
                break;
            case 6:
                cPostre += can;
                break;
        }
    }

    public int calcularTotal() {
        return cHamSen * pHamSen
                + cHamQue * pHamQue
                + cHamEsp * pHamEsp
                + cPapas * pPapas
                + cRefresco * pRefresco
                + cPostre * pPostre;
    }
}
