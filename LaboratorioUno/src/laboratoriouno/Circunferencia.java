/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Circunferencia {

    private double radio;

    public Circunferencia() {
    }

    public Circunferencia(double radio) {
        this.radio = radio;
    }

    /**
     * Calcula el area de una circunferencia
     *
     * @return double area de al circunferencia.
     */
    public double calcularArea() {
        return Math.PI * Math.pow(radio, 2);
    }
    /**
     * Calcula el perimetro de la circunferencia
     * @return double perimetro de la circunferencia
     */
    public double calcularPerimetro() {
        return 2 * Math.PI * radio;
    }

    public double getRadio() {                     
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

}
