/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriodos;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class LaboratorioDos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final int COSTO_X_HORA = 30;
        final int MIN_X_M2 = 10;

        String titulo = "Pintor UTN - v0.1";
        String menu1 = "1. Nueva cotización\n"
                + "2. Salir";

        String menu2 = "Desea agregar otra pared: \n"
                + "1. SI\n"
                + "2. NO";

        String menu3 = "Desea agregar una u otra ventana a la pared #%d \n"
                + "1. SI\n"
                + "2. NO";

        String menu4 = "Tipo de la ventana #%d\n"
                + "1. Rectangular/Cuadrada\n"
                + "2. Circular";

        String resutaldo = "Cliente: %s\n\n"
                + "Área Paredes:  %.2f\n"
                + "Área Ventanas: %.2f\n"
                + "Área a Pintar: %.2f\n\n"
                + "Tiempo Aproximado: $%d dias y %dhrs\n"
                + "Costo: $%d";

        while (true) {
            int op = Util.leerIntJOP(menu1, titulo);
            if (op == 2) {
                Util.mostrarJOP("Gracias por utilizar la aplicación!!");
                break;
            } else if (op == 1) {
                String cliente = Util.leerStringJOP("Digite el nombre del cliente", titulo);
                double tParedes = 0;
                double tVentanas = 0;

                int canParedes = 1;
                while (true) {
                    double ancho = Util.leerDoublesJOP("Digite el ancho de la pared #" + canParedes, titulo);
                    double largo = Util.leerDoublesJOP("Digite el alto de la pared #" + canParedes, titulo);
                    Rectangulo pared = new Rectangulo(largo, ancho);
                    tParedes += pared.calcularArea();

                    //Ventanas
                    int canVentanas = 1;
                    while (true) {
                        op = Util.leerIntJOP(String.format(menu3, canParedes), titulo);
                        if (op == 2) {
                            break;
                        } else if (op == 1) {
                            op = Util.leerIntJOP(String.format(menu4, canVentanas), titulo);
                            if (op == 1) {
                                ancho = Util.leerDoublesJOP("Digite el ancho de la ventana #" + canVentanas, titulo);
                                largo = Util.leerDoublesJOP("Digite el alto de la ventana #" + canVentanas, titulo);
                                Rectangulo v = new Rectangulo(largo, ancho);
                                tVentanas += v.calcularArea();
                            } else {
                                ancho = Util.leerDoublesJOP("Digite el ancho(diametro) de la ventana #" + canVentanas, titulo);
                                Circunferencia v = new Circunferencia(ancho / 2);
                                tVentanas += v.calcularArea();
                            }
                            canVentanas++;
                        }
                    }

                    op = Util.leerIntJOP(menu2, titulo);
                    if (op == 2) {
                        break;
                    }
                    canParedes++;
                }

                double tPintar = tParedes - tVentanas;
                double tiempo = tPintar * MIN_X_M2;
                int hrs = (int) Math.round(tiempo / 60 + 0.5);
                int costo = hrs * COSTO_X_HORA;
                String str = String.format(resutaldo, cliente, tParedes, tVentanas,
                        tPintar, hrs / 24, hrs % 24, costo);
                Util.mostrarJOP(str);

            }
        }

        // TODO code application logic here
        // Nueva Cotización
        // Datos Pared
        // Si hay ventana 
        //Tipo tipo de la ventana
        //Datos de la ventana
        //Hay otra pared
        //Mostramos los datos
        //Salir
    }

}
