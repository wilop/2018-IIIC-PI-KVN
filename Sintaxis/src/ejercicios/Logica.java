/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

/**
 *
 * @author ALLAN
 */
public class Logica {

    public String ejercicioUno(int num) {
        String res = "";
        if (num >= 0) {
            res += "El número " + num + " es positivo.\n";
        } else {
            res += "El número " + num + " es negativo.\n";
        }

        if (num % 5 == 0) {
            res += "El número " + num + ", es divisible entre 5.\n";
        }
        return res;
    }

    /**
     * Genera un número aleatorio del 1 al 6
     *
     * @return numero aleatorio
     */
    public int ejercicioDos() {
        int max = 6;
        int min = 1;
        return (int) (Math.random() * max) + min;
    }

    /**
     * Determina el costo de un vehículo con sus impuestos respectivos
     *
     * @param costo int costo sin impuestos del vehículo
     * @param pasajeros int capacidad de ocupantes del vehículo
     * @param ejes int cantidad de ees
     * @return Costo Total del vehículo
     */
    public double ejercicioTres(int costo, int pasajeros, int ejes) {

        double imp = costo * 0.01; //100
        double cT = costo + imp;

        if (pasajeros < 20) {
            cT += imp * 0.01; //1
        } else if (pasajeros >= 20 && pasajeros <= 60) {
            cT += imp * 0.05;
        } else {
            cT += imp * 0.08;//8
        }

        if (ejes == 2) {
            cT += imp * 0.05;
        } else if (ejes == 3) {
            cT += imp * 0.10; //10
        } else if (ejes > 3) {
            cT += imp * 0.15;//15
        }

        return cT;
    }

    public String ejercicioCuatro(double nota) {

//        if (nota >= 90) {
//            return "Sobresaliente";
//        } else if (nota >= 80) {
//            return "Notable";
//        } else if (nota >= 70) {
//            return "Bien";
//        } else {
//            return "Insuficiente";
//        }
        if (nota < 70) {
            return "Insuficiente";
        } else if (nota < 80) {
            return "Bien";
        } else if (nota < 90) {
            return "Notable";
        } else {
            return "Sobresaliente";
        }
    }

    /**
     * Determina el nivel de un músico a partir de las partituras y las
     * canciones
     *
     * @param partituras int cantidad de partituras
     * @param canciones int cantidadad de canciones
     * @return Nivel de músico
     */
    public String ejercicioCinco(int partituras, int canciones) {
//        if (canciones >= 7 && canciones <= 10 && partituras == 0) {
//            return "Músico Naciente";
//        } else if (canciones >= 7 && canciones <= 10 && partituras >= 1 && partituras <= 5) {
//            return "Músico Estelar";
//        } else if (canciones > 10 && partituras > 5) {
//            return "Músico Consagrado";
//        } else {
//            return "Músico en Formación";
//        }

        if (canciones > 10 && partituras > 5) {
            return "Músico Consagrado";
        } else if (canciones >= 7 && canciones <= 10) {
            if (partituras == 0) {
                return "Músico Naciente";
            } else if (partituras >= 1 && partituras <= 5) {
                return "Músico Estelar";
            } else {
                return "Músico en Formación";
            }
        } else {
            return "Músico en Formación";
        }
    }

    /**
     * Desglosa un monto en las distintas denominaciones disponibles
     *
     * @param monto int a retirar
     * @return información del desglose
     */
    public String ejercicioSeis(int monto) {
        String res = "";

        int moneda = 500;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "billete" : "billetes";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
            //monto -= b * moneda;
        }
        moneda = 200;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "billete" : "billetes";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 100;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "billete" : "billetes";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 50;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "billete" : "billetes";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 20;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "billete" : "billetes";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 10;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "billete" : "billetes";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 5;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "billete" : "billetes";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
        }
        moneda = 2;
        if (monto >= moneda) {
            int b = monto / moneda;
            String texto = b == 1 ? "moneda" : "monedas";
            res += b + " " + texto + " de " + moneda + "\n";
            monto %= moneda;
        }
        if (monto
                > 0) {
            res += monto + " moneda de 1";
        }
        return res;
    }

}
