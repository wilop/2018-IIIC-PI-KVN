/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garrobo;

/**
 *
 * @author ALLAN
 */
public class PruebaGarrobo {
    public static void main(String[] args) {
        Garrobo g1 = new Garrobo();
        Garrobo g2 = new Garrobo();
        g1.nombre = "Juan";
        g1.distancia = 10;
        g1.tiempo = 3;
        g2.nombre = "Pedro";
        g2.distancia = 7;
        g2.tiempo = 2;
        System.out.println(g1.calcularTiempo(3.34));
        System.out.println(g1.calcularDistancia(1));
        System.out.println(g2.calcularTiempo(3.34));
        System.out.println(g2.calcularDistancia(1));
    }
}
