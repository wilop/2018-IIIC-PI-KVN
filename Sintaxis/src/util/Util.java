/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Util {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        int res = Integer.parseInt(sc.nextLine());
        return res;
    }

    public static int leerIntJP(String mensaje) {
        int res = Integer.parseInt(JOptionPane.showInputDialog(mensaje));
        return res;
    }

    public static char leerChar(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        char res = sc.nextLine().charAt(0);//
        return res;
    }
}
