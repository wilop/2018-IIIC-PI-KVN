/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresionesbooleanas;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class EstructurasControl {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite un #: ");
        int num = Integer.parseInt(sc.nextLine()); //Pueden usar el método de nextInt()
        //int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un #: "));
        System.out.println("Instrucción 1");
        if (num == 4) {
            System.out.println("Instrucción 2");
        } else {
            System.out.println("Instrucción 4");
        }
        System.out.println("Instrucción 3");

        if (num % 2 == 0) {
            System.out.println("El número " + num + " es par");
        } else {
            String res = String.format("El número %d es impar", num);
            System.out.println(res);
        }

        System.out.print("Digite el número 1 : ");
        int x = Integer.parseInt(sc.nextLine()); //Pueden usar el método de nextInt()
        System.out.print("Digite el número 2 : ");
        int y = Integer.parseInt(sc.nextLine()); //Pueden usar el método de nextInt()

        if (x < y) {
            String res = String.format("La variable x=%d es menor que y=%d", x, y);
            System.out.println(res);
        } else if (x > y) {
            System.out.println("La variable x=" + x + " es mayor que y=" + y);
        } else {
            System.out.println(String.format("Las variables x = %d, y = %d, son iguales", x, y));
        }

    }
}
