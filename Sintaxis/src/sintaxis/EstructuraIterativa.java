/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class EstructuraIterativa {

    public static void main(String[] args) {

//        int con = 1;
//        while (con <= 10) {
//            System.out.println(con);
//            con++;
//        }
//        con = 10;
//        while (con >= 1) {
//            System.out.println(con);
//            con--;
//        }
//
//        int tabla = Util.leerInt("Tabla");
//
//        con = 1;
//        while (con <= 10) {
//            String res = String.format("%dx%d=%d", tabla, con, (tabla * con));
//            System.out.println(res);
////            System.out.println(tabla + "x" + con + "=" + (tabla * con));
//            con++;
//        }
//
//        con = 1;
//        int sum = 0;
//        String tex = "";
//        while (con <= 10) {
//            int num = Util.leerInt("#" + con);
//            if (num < 0) {
//                break;
//            }
//            sum += num;
//            tex += num + "+";
//            con++;
//        }
//        System.out.println(tex + "\b=" + sum);
//        int x = 1;
//        System.out.println("WHILE");
//        while (x < 1) {
//            System.out.println(x);
//            x++;
//        }
//        System.out.println("DO");
//        x = 1;
//        do {
//            System.out.println(x);
//            x++;
//        } while (x < 1);
//        int tabla = Util.leerInt("Tabla");
//        for (int i = 1; i <= 10; i++) {
//            String res = String.format("%dx%d=%d", tabla, i, (tabla * i));
//            System.out.println(res);
//        }
        for (int i = 1; i <= 1000; i++) {
            if (i % 7 == 0) {
                System.out.println(i);
            }
        }

    }
}
