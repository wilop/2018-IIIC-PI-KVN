/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class OtroNumero {

    private int numero;

    public OtroNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Determina sin es primo o es compuesto
     *
     * @return boolean true cuando es primo y false cuando es compuesto
     */
    public boolean esPrimo() {
        int div = 0;
        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                div++;
            }
            if (div > 2) {
                return false;
            }
        }
        return div == 2;
    }

    /**
     * Determina sin un número es perfecto, abundante o deficiente.
     *
     * @return int 1 es perfecto, 2 deficiente, 3 es abundante
     */
    public int tipoNumero() {
        int div = 0;
        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                div += i;
            }
        }
        return div == numero ? 1 : div < numero ? 2 : 3;

    }

    public String getTipo(int tipo) {
        switch (tipo) {
            case 1:
                return "Perfecto";
            case 2:
                return "Deficiente";
            default:
                return "Abundante";
        }
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

}
