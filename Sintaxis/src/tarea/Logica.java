/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;


/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Determina el costo final y el descuento que aplique para el viaje
     *
     * @param can int Cantidad de Viajeros
     * @param pre int Precio por persona
     * @param edad int edad de la persona que viaja sola
     * @return String detalle de los costos
     */
    public String tareaUno(int can, int pre, int edad) {
        if (can > 0) {
            if (can == 1) {//Solos
                if (edad >= 18 && edad <= 30) {
                    return calcularTotal(pre, 0.078, can);
                } else if (edad > 30) {
                    return calcularTotal(pre, 0.1, can);
                } else {
                    return String.format("Total: %d", pre);
                }
            } else if (can == 2) {//Pareja
                return calcularTotal(pre, 0.115, can);
            } else if (can > 3) {//+3
                return calcularTotal(pre, 0.15, can);
            } else {//3
                return String.format("Total: %d", pre * can);
            }
        } else {
            return "Cantidad Inválida";
        }
    }

    private String calcularTotal(int pre, double porc, int can) {
        double desc = pre * can * porc;
        double pf = pre * can - desc;
        String res = String.format("Subtotal: %d\n"
                + "    Desc: %.2f\n"
                + "   Total: %.2f", pre * can, desc, pf);
        return res;
    }
}
