/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class TareaDos {

    public static void main(String[] args) {
        OtroNumero tarea = null;
        String menu = "\nTarea Dos - v.01\n"
                + "1. Digitar un número\n"
                + "2. Primo/Compuesto\n"
                + "3. Tipo número\n"
                + "4. Salir\n"
                + "Digite una opción";
        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    tarea = new OtroNumero(Util.leerInt("Digite un número"));
                    break;
                case 2:
                    if (tarea == null) {
                        System.out.println("Favor digite un número (opción 1)");
                    } else {
                        System.out.println(tarea.esPrimo()
                                ? "Es primo"
                                : "Es compuesto");
                    }
                    break;
                case 3:
                    if (tarea == null) {
                        System.out.println("Favor digite un número (opción 1)");
                    } else {
                        int res = tarea.tipoNumero();
                        System.out.println(tarea.getTipo(res));
                    }
                    break;
                case 4:
                    break APP;
                default:
                    System.out.println("Opción Inválida ");
            }
        } while (true);

    }
}
