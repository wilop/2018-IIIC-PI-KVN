/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
        //Generar el número
        Juego j = new Juego();
        //Ciclo
        for (int i = 1; i <= 5; i++) {
            //Pedir el número
            int num = Util.leerInt("Digite un número");
            j.jugar(num);
            //Gano
            if (j.isGano()) {
                System.out.println("Ganó");
                break;
            } else if (j.perdio()) {             //Perdió

                System.out.println("Perdió");
                System.out.println("El número era: " + j.getAleatorio());
                break;
            } else {
                //Pista
                System.out.println(j.pista(num));
            }
        }
    }
}
